// xredis
package main

import (
	"color"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gosexy/redis"
	"strings"
	"time"
)

func GetInfo(c *redis.Client, sec string) map[string]string {
	s, err := c.Info(sec)
	if err != nil {
		fmt.Println("get key failure")
		return nil
	}
	a1 := strings.Split(s, "\r\n")
	m1 := make(map[string]string)
	for i := 1; i < len(a1); i++ {
		if !strings.Contains(a1[i], ":") {
			continue
		}
		a2 := strings.Split(a1[i], ":")
		m1[a2[0]] = a2[1]
	}
	return m1
}

func Help() {
	color.Cyan("Version: xredis_v1.0\n")
	fmt.Printf("Usage:./xredis\n")
	fmt.Printf("Usage:./xredis --i=\"192.168.1.1\" -p=\"6379\" -w=\"password\"\n")
	fmt.Printf("Usage:./xredis -j=Server args is (Server|Clients|Memory|Persistence|Stats|Replication|CPU)\n")
	fmt.Printf("Usage:./xredis -k=Server args is (Server|Clients|Memory|Persistence|Stats|Replication|CPU)\n")
	fmt.Printf("Usage:./xredis -c=false show color\n")
	fmt.Printf("defautl ip=127.0.0.1 default port=6379\n")
}

func main() {
	help := flag.Bool("h", false, "show help")
	ip := flag.String("i", "127.0.0.1", "redis server ip")
	port := flag.Uint("p", 6379, "redis server port")
	pass := flag.String("w", "", "redis server password")
	showColor := flag.Bool("c", true, "show or not show color")
	xjson := flag.String("j", "", "Server|Clients|Memory|Persistence|Stats|Replication|CPU")
	xkey := flag.String("k", "", "Server|Clients|Memory|Persistence|Stats|Replication|CPU")
	flag.Parse()

	if *showColor {
		color.Enable()
	}

	if *help {
		Help()
		return
	}

	T_out := time.Duration(time.Second * 10) //connect redis timeout

	client := redis.New()
	err := client.ConnectWithTimeout(*ip, *port, T_out)
	if err != nil {
		fmt.Println("connect redis-server failure")
		return
	}
	//redis auth
	if *pass != "" {
		ss, _ := client.Auth(*pass)
		if ss != "OK" {
			fmt.Println(ss)
		}
	}

	if *xjson != "" {
		mm := GetInfo(client, *xjson)
		b, _ := json.Marshal(mm)
		fmt.Println(string(b))
		return
	}
	if *xkey != "" {
		mm := GetInfo(client, *xkey)
		for k, v := range mm {
			fmt.Printf("%s=%s\n", k, v)
		}
		return
	}

	m1 := GetInfo(client, "Server")
	m2 := GetInfo(client, "Clients")
	m3 := GetInfo(client, "Memory")
	//m4 := GetInfo(client, "Persistence")
	m5 := GetInfo(client, "Stats")
	m6 := GetInfo(client, "Replication")
	m7 := GetInfo(client, "CPU")
	client.Quit()
	color.Cyan("  redis_version             : %-12s", m1["redis_version"])
	color.Green("|   redis_mode                : %s\n", m1["redis_mode"])
	color.Cyan("  uptime_days               : %-12s", m1["uptime_in_days"])
	color.Green("|   config_file               : %s\n", m1["config_file"])
	color.Cyan("  connected_clients         : %-12s", m2["connected_clients"])
	color.Green("|   blocked_clients           : %s\n", m2["blocked_clients"])
	color.Cyan("  used_memory               : %-12s", m3["used_memory_human"])
	color.Green("|   used_memory_peak          : %s\n", m3["used_memory_peak_human"])
	color.Cyan("  input_bytes               : %-12s", m5["total_net_input_bytes"])
	color.Green("|   output_bytes              : %s\n", m5["total_net_output_bytes"])
	color.Cyan("  instantaneous_input_kbps  : %-12s", m5["instantaneous_input_kbps"])
	color.Green("|   instantaneous_output_kbps : %s\n", m5["instantaneous_output_kbps"])
	color.Cyan("  expired_keys              : %-12s", m5["expired_keys"])
	color.Green("|   evicted_keys              : %s\n", m5["evicted_keys"])
	color.Cyan("  role                      : %-12s", m6["role"])
	color.Green("|   connected_slaves          : %s\n", m6["connected_slaves"])
	color.Cyan("  used_cpu_sys              : %-12s", m7["used_cpu_sys"])
	color.Green("|   used_cpu_user             : %s\n", m7["used_cpu_user"])
}
